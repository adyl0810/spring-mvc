package com.attractorschool.homework.controller;

import com.attractorschool.homework.model.AnimalRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AnimalController {
    private final AnimalRepository repository;

    public AnimalController(AnimalRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/animals")
    public String animals(Model model) {
        model.addAttribute("animals", repository.findAll());
        return "animals";
    }
}

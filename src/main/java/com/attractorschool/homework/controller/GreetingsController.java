package com.attractorschool.homework.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class GreetingsController {

    @GetMapping("/greetings")
    public String greetingsHandler() {
        return "greetings";
    }
}

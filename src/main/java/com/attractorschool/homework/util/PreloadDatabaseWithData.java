package com.attractorschool.homework.util;

import com.attractorschool.homework.model.Animal;
import com.attractorschool.homework.model.AnimalRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.stream.Stream;

@Configuration
public class PreloadDatabaseWithData {

    @Bean
    CommandLineRunner initDatabaseAnimals(AnimalRepository repository) {

        repository.deleteAll();

        return (args) -> Stream.of(animals())
                .peek(System.out::println)
                .forEach(repository::save);
    }

    private Animal[] animals() {
        return new Animal[] {
                new Animal("Dog", "dog.jpg"),
                new Animal("Cat", "cat.jpg"),
                new Animal("Axolotl", "axolotl.jpg"),
                new Animal("Weasel", "weasel.jpg")
        };
    }

}
